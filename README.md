# Cryptograph

There is such a complex universe booming around crypto-enabled digital assets
that everytime I am trying to have a complete picture I end up with an
incomplete static image and even more questions.

So many tokens for so many different purposes for which I'd love to see a map.

So here I propose a dynamic content and visualization, so that:
 - content can be searched and visualized on [cryptograph.red](https://cryptograph.red)
 - content can be modified, corrected and added by making a merge request

You are more than welcome to contribute as the dataset is licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>
<br /><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

keywords: blockchain, token, cryptoasset, exchange, dapp, smart contracts


